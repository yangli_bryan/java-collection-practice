import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;

public class ReduceCollection {
    public static int getMax(List<Integer> list) {
        return list.stream().reduce(0, (maxValue, value) -> maxValue > value ? maxValue : value);
    }
    
    public static double getAverage(List<Integer> list) {
        return (double) list.stream().reduce(0, Integer::sum) / list.size();
    }
    
    public static int getSum(List<Integer> list) {
        return list.stream().reduce(0, Integer::sum);
    }
    
    public static double getMedian(List<Integer> list) {
        List<Integer> bucket = new ArrayList<>();
        list.forEach( x -> {
            if (Math.abs((list.stream().filter(y -> y < x).count() -
                          list.stream().filter(y -> y > x).count())) <= 1) {
                bucket.add(x);
            }
        });
        return (double) bucket.stream().reduce(0, Integer::sum) / bucket.size();
    }
    
    public static int getFirstEven(List<Integer> list) {
        return list.stream().reduce(getMax(list), (firstEven, value) ->
                (value % 2 == 0 && value < firstEven) ? value : firstEven);
    }
}
