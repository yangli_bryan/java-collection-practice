import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectionOperation {
    
    public static List<Integer> getListByInterval(int left, int right) {
        if (left < right) {
            return Stream.iterate(left, n -> n + 1).limit(right - left + 1).collect(Collectors.toList());
        } else {
            return Stream.iterate(left, n -> n - 1).limit(left - right + 1).collect(Collectors.toList());
        }
    }
    
    
    public static List<Integer> removeLastElement(List<Integer> list) {
        return list.stream().limit(list.size() - 1).collect(Collectors.toList());
    }
    
    public static List<Integer> sortDesc(List<Integer> list) {
        return list.stream().sorted((a, b) ->  b - a).collect(Collectors.toList());
    }
    
    
    public static List<Integer> reverseList(List<Integer> list) {
        Collections.reverse(list);
        return list;
    }
    
    
    public static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
        return Stream.concat(list1.stream(), list2.stream()).collect(Collectors.toList());
    }
    
    public static List<Integer> union(List<Integer> list1, List<Integer> list2) {
        return concat(list1, list2).stream().distinct().collect(Collectors.toList());
    }
    
    public static boolean isAllElementsEqual(List<Integer> list1, List<Integer> list2) {
        return sortDesc(list1).equals(sortDesc(list2));
    }
}
